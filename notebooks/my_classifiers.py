from math import sqrt, log2, exp, pi 
from enum import Enum
from typing import Any
import pandas as pd
import numpy as np

def max_classes(dictClasses:dict) -> list:
  # Ищем максимум по значению
  max_count_class = max(list(dictClasses.values()))
  # Ищем классы которые встречаются максимальное кол-во раз
  find_classes = []
  for classs, count in dictClasses.items():
    if count == max_count_class:
      find_classes.append(classs)
  return find_classes

  
def unique_class_dict(x) -> dict:
  list_class = list(set(x))
  temp = list(map(lambda k: x.count(k), list_class))
  dict_classes = dict(zip(list_class, temp))
  return dict_classes

##===========- KNN -=================

def EuclideanMetric(x:list, y:list) -> float:
  x = np.array(x)
  y = np.array(y)
  return sqrt(np.sum(((x-y)**2)))

class MyKNeighborsClassifier:
  def __init__(self, n_neighbors:int = 5, metriс = EuclideanMetric) -> None:
    self.n = n_neighbors
    self.is_even = n_neighbors % 2 == 0
    self.metriс = metriс
  
  def fit(self, X, y)-> None:
    self.fit_X = np.array(X)
    self.fit_y = np.array(y)

  def predict(self, X_test)-> list:
    result = []
    for obj in X_test:
      # Вычисляем дистанцию от каждого объекта до одного из выборки
      dist = np.array(list(map((lambda x: self.metriс(x, y = obj)), self.fit_X)))
      # Превращаем в словарь полученный массив дистанций
      order_dist = dict(zip(list(range(0, len(self.fit_X) )), dist))
      # Сортируем словарь по значению
      sort_order_dist = sorted(order_dist.items(), key = lambda item: item[1])
      # Отсекаем первые n элементов
      near_obj_dist = sort_order_dist[0:self.n:]
      # Делаем список индексов этих эелементов
      indexes = list(map(lambda x: x[0], near_obj_dist))
      # Делаем словарь этих объектов по типу: индекс : класс
      near_obj_y = dict(zip(indexes, list(map(lambda x: self.fit_y[x], indexes))))
      # Делаем словарь по типу: уникальный класс/кол-во объектов
      dict_classes = unique_class_dict(list(near_obj_y.values()))
      # Ищем максимальные классы
      find_classes = max_classes(dict_classes)

      # Если такой класс один то просто возвращаем
      if(len(find_classes) == 1):
        result.append(find_classes[0])
      else:
        # Если такой класс не один
        # то ищем по отсортированному словарю близь лежащих объектов начиная с конца
        # и если самый близкий объект имеет класс, который максимальный по числу то возвращаем его
        for index,obj in near_obj_dist:
          if near_obj_y[index] in find_classes:
            result.append(near_obj_y[index])
            break

    return result

  def __del__(self)->None:
    pass

#================- DecisionTreeClassifier -================

class NodeType(Enum):
  LEAF = 1
  BRANCH = 2

def entropy2(x) -> float:
  dict_classes = unique_class_dict(x)
  list_class = list(set(x))
  count = len(x)
  result = 0
  for classs in list_class:
    p_i = (dict_classes[classs]/count)
    result -= p_i*log2(p_i)
  return result

def info_gain(root,left,right) -> float:
  d = len(root)
  return entropy2(root) - len(left)/d*entropy2(left) - len(right)/d*entropy2(right)

def solution_for_gain(space) -> tuple:
  index_attr = 0
  value_attr = 0
  min_entropy = float('inf')
  for i in range(len(space)):
    for value, entropy in space[i].items():
      if min_entropy > entropy:
        index_attr = i
        value_attr = value
        min_entropy = entropy
  return (index_attr, value_attr)

class NodeDT:
  def __init__(self, Type:NodeType, data, metric:float, children:list=[]) -> None:
      self.Type = Type
      self.data = data
      self.metric = metric
      self.children =  children

  def predict(self, x) -> None:
    pass

class LeafDT(NodeDT):
  def __init__(self, data, metric:float, classs) -> None:
    super().__init__(NodeType.LEAF, data, metric)
    self.classs = classs

  def predict(self, x) -> Any:
    return self.classs

class BranchDT(NodeDT):
  def __init__(self, data, metric: float, attr_index, attr_value, children: list) -> None:
      super().__init__(NodeType.BRANCH, data, metric, children=children)
      self.attr_index = attr_index
      self.attr_value = attr_value

  def predict(self, x) -> Any:
    if(x[self.attr_index]<= self.attr_value):
      return self.children[0].predict(x)
    else:
      return self.children[1].predict(x)

class MyDecisionTreeClassifier:
  def __init__(self,scope = info_gain, metric = entropy2, solution = solution_for_gain, deep=-1, partition=5) -> None:
    self.metric = metric
    self.scope = scope
    self.solution = solution
    self.root = None
    self.deep = deep
    self.partition = partition

  def predict(self, X_test:list) -> Any:
    return list(map(lambda x:self.root.predict(x), X_test))

  def fit(self, X_train, y_train) -> None:
    order = list(range(len(y_train)))
    y_series = pd.Series(y_train,index = order)
    self.__data = pd.DataFrame(X_train)
    self.__data['y'] = y_series
    self.root = self.__create_Node__(self.__data, self.deep)

  def __create_Node__(self, data, deep) -> Any:
    # Выделяем y столбец
    y_column = data['y'].values.tolist()
    # Создаем словарь по типу : класс/кол-во элементов класса
    dict_classes = unique_class_dict(y_column)

    # Если в данных приссутствую объекы  только одного класса - создаем иозвращаем лист
    if(len(dict_classes) == 1):
      return self.__create_leaf__(data, dict_classes)
    # Если deep = 1, мы тостигли указанной глубины и вместо ветвления просто возвращаем лист
    if(deep == 1):
      return self.__create_leaf__(data, dict_classes)

    # Создаем массив(Series) мин макс значений для каждого признака
    temp_x = data.drop(['y'], axis=1)
    mins = list(temp_x.min(axis=0))
    maxes = list(temp_x.max(axis=0))
    # Считаем метрику тек. данных
    metric = self.metric(y_column)

    # Саздаем массив словарей
    # где индекс -  индекс признака
    # значение - словарь по типу: значение разбиение / оценка
    space_attr = []
    # Цикл на каждый атрибут
    for i in range(len(data.columns) - 1):
      # Разбиваем отрезок от мин до мак значения i-ого признака и представляем в виде массива 
      lin_space, step = np.linspace(mins[i], maxes[i], self.partition,endpoint = False, retstep = True)
      lin_space = list(lin_space)
      metrics = []
      # перебираем полученные значения
      for j in range(self.partition):
        # разбиваем данные
        root, left, right = self.__split_y__(data,i,lin_space[j])
        # оцениваем их
        temp = self.scope(root, left, right)
        # записываем в массив
        metrics.append(temp)
      # создаем словарь по типу: значение разбиение / оценка
      temp = dict(zip(lin_space,metrics))
      # и записываем в массив словарей
      space_attr.append(temp)
      
    is_right_solution = False
    while is_right_solution == False:
      # Вызываем фукцию оценки и получаем индекс признака и значение по которому разбиваем
      index_attr, index_value = self.solution(space_attr)
      # Разбиваем сами данные на левую ветвь и правую
      left_data = data[data[index_attr] <= index_value]
      right_data = data[data[index_attr] > index_value]
      if ((left_data.size == 0) or (right_data.size == 0)):
        space_attr[index_attr].pop(index_value)
      else:
        is_right_solution = True

    # Создаем дочерние узлы
    left_node = self.__create_Node__(left_data, deep - 1)
    right_node = self.__create_Node__(right_data, deep - 1)
    # Создаем и возвращаем узел
    return BranchDT(dict_classes,
                      self.metric(y_column),
                      index_attr,
                      index_value,
                      [left_node, right_node])

      
  def __split_y__(self, data, attr_index:int, attr_value:float) -> tuple:
    root  = data['y'].values.tolist()
    left = (data[data[attr_index] <= attr_value])['y'].values.tolist()
    right = (data[data[attr_index] > attr_value])['y'].values.tolist()
    return (root,left,right)

  def __create_leaf__(self, data, dict_class: dict) -> LeafDT:
      # Считаем метрику
      metric = self.metric(data['y'].values.tolist())
      # Находим максимальные по кол-ву класcы
      max_count_class = max_classes(dict_class)
      # Возвращаем лист. Если макисмальных классов несколько, то у них одно и тоже кол-во и вероятность отношения к тому или иному классу
      # Равнозначна, поэтому выдаем первый
      return LeafDT(dict_class, metric, max_count_class[0])


#================- GaussianNB -==================

def probabilityDensityFunction(std, mean) -> Any:
  if(std == 0)
    std == 1e-10  
  def func(x):
    return (1 / (std * sqrt(2*pi))) * exp(-((x - mean)**2) / (2 * (std ** 2)))
  return func

class MyNaiveBayesClassifier:
  def __init__(self) -> None:
    pass
  
  def fit(self, X_train, y_train) -> None:
    # Создаем Серию из ответов
    y_series = pd.Series(y_train)
    # Создаем таблицу из матрицы признаков
    self.__data = pd.DataFrame(X_train)
    # Выделяем массив признаков
    self.__attrs = self.__data.columns.tolist()
    # Добавляем к таблице колонку ответов
    self.__data['y'] = y_series
    # Выделяем классы ответов
    self.__classes = list(set(y_train))
    # Создаем таблицу, где колонки = виды классов, индексы = аттрибуты
    self.__frequency_table = pd.DataFrame(columns = self.__classes, index = self.__attrs)
    # Перебираем все индексы и классы
    for attr in self.__attrs:
      for classs in self.__classes:
        # Высчитываем среднее значение и стандартное отклонение для значений атрибутов при принадлежности к классу
        mean = self.__data.loc[self.__data['y'] == classs][attr].mean()
        std = self.__data.loc[self.__data['y'] == classs][attr].std()
        # В ячейку записывам функцию плотности
        self.__frequency_table.loc[attr,classs] = probabilityDensityFunction(std,mean)

  def predict(self, X_test) -> list:
    # Создаем лист ответов
    result = []
    # Проходимся по объектам
    for obj in X_test:
      # Создаем словарь вероятностей для текущего объекта по типу: класс/вероятность принадлежности
      probability_dict = dict()
      # Проходимся по классам
      for classs in self.__classes:
      # Изначально вероятность принадлежности к текущему классу равна единице
        likelihood = 1
        # Проходимся по всем атрибутам
        for attr in self.__attrs:
          # Выделяем функцию плотности для данного атрибута при принадлежности к классу classs
          # И умножаем вероятность класса на значение функции от значения объекта по текущему атрибуту
          probabillity_func = self.__frequency_table.loc[attr,classs]
          likelihood *= probabillity_func(obj[attr])
        # Записываем вероятность в словарь
        probability_dict[classs] = likelihood
      # Находим максимальные классы по вероятности
      variant_classes = max_classes(probability_dict)
      # Записываем самый первый класс
      result.append(variant_classes[0])
    return result
