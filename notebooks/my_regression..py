from math import sqrt, log2, exp, pi 
from operator import xor
from enum import Enum
import pandas as pd
import numpy as np

def grad_mse(i_data, w):
  X = i_data.drop(['y'], axis=1).values
  x_shape = X.shape
  y = i_data['y'].values
  temp = np.dot(X, w)
  temp1 = np.sum(np.dot(X.T, (temp - y)),axis=1)
  return (2/x_shape[0] * temp1).reshape(w.shape)

class LinerRegression:
  def __init__(self, n_iter = 1e6, eps = 1e-5, learning_rate=1e-5, grad_f=grad_mse) -> None:
    self.n_iter = n_iter
    self.eps = eps
    self.w = 0
    self.learning_rate = learning_rate
    self.grad_f = grad_f

  def predict(self, X_test) -> list:
      X_data = pd.DataFrame(X_test)
      X_data['b'] = np.ones((X_test.shape[0], 1))
      X_test = X_data.values
      answer = list()
      for obj in X_test:
        answer.append(np.dot(obj, self.w))
      return answer
  
  def fit(self, X_train, y_train) -> None:
    y_series = pd.Series(y_train)

    data = pd.DataFrame(X_train)
    count_obj, count_feature = data.shape
    data['b'] = np.ones((count_obj, 1))
    count_feature = count_feature+1
    data['y'] = y_series

    self.w = np.random.randn(count_feature,1)
  
    time = 0
    while time < self.n_iter:
        self.w = self.w - self.learning_rate * self.grad_f(data, self.w)
        time = time +1


class NodeType(Enum):
  LEAF = 1
  BRANCH = 2

class NodeDT:
  def __init__(self, Type:NodeType, data, metric:float, children:list=[]) -> None:
      self.Type = Type
      self.data = data
      self.metric = metric
      self.children =  children

  def predict(self, x) -> None:
    pass

class LeafDT(NodeDT):
  def __init__(self, data, metric:float, classs) -> None:
    super().__init__(NodeType.LEAF, data, metric)
    self.classs = classs

  def predict(self, x) :
    return self.classs

class BranchDT(NodeDT):
  def __init__(self, data, metric: float, attr_index, attr_value, children: list) -> None:
      super().__init__(NodeType.BRANCH, data, metric, children=children)
      self.attr_index = attr_index
      self.attr_value = attr_value

  def predict(self, x):
    if(x[self.attr_index]<= self.attr_value):
      return self.children[0].predict(x)
    else:
      return self.children[1].predict(x)


def mse_fortree(x) -> float:
  x_temp = np.array(x)
  mse = (((x_temp - x_temp.mean()) ** 2 ) / x_temp.shape[0]).sum()
  return mse

def info_gain(root,left,right) -> float:
  d = len(root)
  return mse_fortree(root)*2 - mse_fortree(left) - mse_fortree(right)

def solution_for_gain(space) -> tuple:
  index_attr = 0
  value_attr = 0
  min_entropy = float('inf')
  for i in range(len(space)):
    for value, entropy in space[i].items():
      if min_entropy > entropy:
        index_attr = i
        value_attr = value
        min_entropy = entropy
  return (index_attr, value_attr)

class MyDecisionTreeRegression:
  def __init__(self,scope = info_gain, metric = mse_fortree, solution = solution_for_gain, deep=-1, partition=5) -> None:
    self.metric = metric
    self.scope = scope
    self.solution = solution
    self.root = None
    self.deep = deep
    self.partition = partition

  def predict(self, X_test:list):
    return list(map(lambda x:self.root.predict(x), X_test))

  def fit(self, X_train, y_train) -> None:
    order = list(range(len(y_train)))
    y_series = pd.Series(y_train,index = order)
    self.__data = pd.DataFrame(X_train)
    self.__data['y'] = y_series
    self.root = self.__create_Node__(self.__data, self.deep)

  def __create_Node__(self, data, deep):
    # Выделяем y столбец
    y_column = data['y'].values.tolist()
    ## Создаем словарь по типу : класс/кол-во элементов класса
    #dict_classes = unique_class_dict(y_column)

    # Если в данных приссутствую объекы  только одного класса - создаем иозвращаем лист
    if(len(y_column) <= 5):
      return self.__create_leaf__(data, len(y_column))
    # Если deep = 1, мы тостигли указанной глубины и вместо ветвления просто возвращаем лист
    if(deep == 1):
      return self.__create_leaf__(data, len(y_column))

    # Создаем массив(Series) мин макс значений для каждого признака
    temp_x = data.drop(['y'], axis=1)
    mins = list(temp_x.min(axis=0))
    maxes = list(temp_x.max(axis=0))
    # Считаем метрику тек. данных
    metric = self.metric(y_column)

    # Саздаем массив словарей
    # где индекс -  индекс признака
    # значение - словарь по типу: значение разбиение / оценка
    space_attr = []
    # Цикл на каждый атрибут
    for i in range(len(data.columns) - 1):
      # Разбиваем отрезок от мин до мак значения i-ого признака и представляем в виде массива 
      lin_space, step = np.linspace(mins[i], maxes[i], self.partition,endpoint = False, retstep = True)
      lin_space = list(lin_space)
      metrics = []
      # перебираем полученные значения
      for j in range(self.partition):
        # разбиваем данные
        root, left, right = self.__split_y__(data,i,lin_space[j])
        # оцениваем их
        temp = self.scope(root, left, right)
        # записываем в массив
        metrics.append(temp)
      # создаем словарь по типу: значение разбиение / оценка
      temp = dict(zip(lin_space,metrics))
      # и записываем в массив словарей
      space_attr.append(temp)
      
    is_right_solution = False
    while is_right_solution == False:
      # Вызываем фукцию оценки и получаем индекс признака и значение по которому разбиваем
      index_attr, index_value = self.solution(space_attr)
      # Разбиваем сами данные на левую ветвь и правую
      left_data = data[data[index_attr] <= index_value]
      right_data = data[data[index_attr] > index_value]
      if ((left_data.size == 0) or (right_data.size == 0)):
        if(len(space_attr)>1):
           space_attr[index_attr].pop(index_value)
        else:
            is_right_solution = True
      else:
        is_right_solution = True

    # Создаем дочерние узлы
    left_node = self.__create_Node__(left_data, deep - 1)
    right_node = self.__create_Node__(right_data, deep - 1)
    # Создаем и возвращаем узел
    return BranchDT(len(y_column),
                      self.metric(y_column),
                      index_attr,
                      index_value,
                      [left_node, right_node])

      
  def __split_y__(self, data, attr_index:int, attr_value:float) -> tuple:
    root  = data['y'].values.tolist()
    left = (data[data[attr_index] <= attr_value])['y'].values.tolist()
    right = (data[data[attr_index] > attr_value])['y'].values.tolist()
    return (root,left,right)

  def __create_leaf__(self, data, dict_class) -> LeafDT:
      # Считаем метрику
      metric = self.metric(data['y'].values.tolist())
      # Находим максимальные по кол-ву класcы
      max_count_class = metric
      # Возвращаем лист. Если макисмальных классов несколько, то у них одно и тоже кол-во и вероятность отношения к тому или иному классу
      # Равнозначна, поэтому выдаем первый
      return LeafDT(dict_class, metric, max_count_class)
